package prueba;

public interface Serie {

		public void numeroInicial(int numero);
		public void numeroInicialDefecte();
		public int seguentNumero();
		
}
