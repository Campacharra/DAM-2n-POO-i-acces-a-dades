package prueba;

import java.util.Comparator;

public class Persona implements Comparable<Persona>{

	private double pes;
	private int edad;
	private double al�ada;
	
	
	//Constructor sense parametres
	public Persona(){
		this(84,25,1.87);
	}
	
	//Constructor amb parametres
	public Persona(double pesa, int edada, double al�adaa){
		//Atributs de persona
		this.pes = pesa;
		this.edad = edada;
		this.al�ada = al�adaa;
		
	}

	public double getPes() {
		return pes;
	}

	public void setPes(double pes) {
		this.pes = pes;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getAl�ada() {
		return al�ada;
	}

	public void setAl�ada(double al�ada) {
		this.al�ada = al�ada;
	}
	
	
	
	
	//Comparador edad
	public static final Comparator<Persona> COMPARATO_AGE = new Comparator<Persona>(){
		@Override
		public int compare(Persona p1, Persona p2) {
			
			if(p1.getEdad() > p2.getEdad()){
				return 1;
			}
			else if(p1.getEdad() < p2.getEdad()){
				return -1;
			}
			else{
				return 0;
			}
		}
	};
	
	
	//Comparador pes
	public static final Comparator<Persona> COMPARATO_WEIGHT = new Comparator<Persona>(){
		@Override
		public int compare(Persona p1, Persona p2) {
			
			if(p1.getPes() > p2.getPes()){
				return 1;
			}
			else if(p1.getPes() < p2.getPes()){
				return -1;
			}
			else{
				return 0;
			}
		}
	};
	
	
	//Comparador Al�ada
	

	@Override
	public int compareTo(Persona o) {
		
		if(this.getAl�ada() > o.getAl�ada()){
			return 1;
		}
		else if(this.getAl�ada() < o.getAl�ada()){
			return -1;
		}
		else{
			return 0;
		}
	}




}
