
public class Main {

	public static void main(String[] args) {

		Tirada objectproba1 = new Tirada(5,3,4);
		Tirada objectproba2 = new Tirada(5,3,4);
		System.out.println("******************Player 1******************\n");
		System.out.println(objectproba1.toString());
		System.out.println("\n******************Player 2******************\n");
		System.out.println(objectproba2.toString());
		System.out.println("\n******************Comparable throws******************\n");
		System.out.println(objectproba1.compareTo(objectproba2));
		System.out.println("\n******************Cloneable throws******************\n");
		Tirada objectproba3 = objectproba2.clone();
		System.out.println(objectproba3.compareTo(objectproba2));
		

	}

}
