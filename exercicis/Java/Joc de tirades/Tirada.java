import java.util.Arrays;


public class Tirada implements Comparable<Tirada>, Cloneable{

	
	private int atack;
	private int defense;
	private int dice;
	private int[] diceVector;
	
	public Tirada(int atack1, int defense1, int numberdice){
		
		this.atack = atack1;
		this.defense = defense1;
		this.dice = numberdice;
		diceVector = new int[numberdice];
		
		/* Bucle for random number */
		
		/*numbers */
		for(int i=0;i<dice;i++){
			diceVector[i] = (int)(Math.random() * 6) + 1; 
		/*System.out.print("\n" + diceVector[i]);*/
		};
	}
	
	/* Get of atributes */
	public int getAtack(){
		return atack;
	}

	public int getDefense(){
		return defense;
	}

	public int[] getDice(){
		return diceVector;
	}
	
	
	/* ToString to show in String format */
	public String toString() {
		
		String str = String.format("AT: %d  DEF: %d  Dice (%d): ", getAtack(), getDefense(), diceVector.length);
		/*Storage numbers*/
		for (int i=0; i<diceVector.length; i++)
			str = str + diceVector[i];
		
		return str;
	}

	
	/* Comparable two throw dice */
	
	@Override
	public int compareTo(Tirada objectproba1) {
		
		int ver1=0;
		int ver2=0;
		int attack1,defense1,attack2,defense2;
		
		/* Step 1 - Case 1 */
		if(diceVector.length > objectproba1.diceVector.length){
			if(diceVector.length>objectproba1.diceVector.length){
				for (int i=0; i<diceVector.length-objectproba1.diceVector.length; i++){
					if(diceVector[i]+atack > objectproba1.defense){
						ver1++;
					}
				}
			}	
		}
		/* Step 2 - Case 2 */
	if(diceVector.length < objectproba1.diceVector.length){
			
			if(diceVector.length>objectproba1.diceVector.length){
				for (int j=0; j<objectproba1.diceVector.length-diceVector.length; j++){
					if(objectproba1.diceVector[j]+atack > defense){
						ver2++;
					}
				}
			}	
		}
	/* Comparable atacks */
	for (int i=0; i <diceVector.length; i++){
		for(int j=0;j<diceVector.length;j++){
			attack1 = diceVector[i]+atack;
			attack2 = objectproba1.diceVector[j]+atack;
			defense1 = diceVector[i]+defense;
			defense2 = objectproba1.diceVector[j]+defense;
		
		/*Comparable - Case 1 OR Case 2*/
		if(attack1 > defense2)
			ver1++;
		if(attack2 > defense1)
			ver2++;
		}
	}
	/*Return response*/
	return ver1-ver2;
	}
	
	/* Clonable*/
	
	public Tirada clone(){
		Tirada proba910 = null;
		try{
			proba910=(Tirada) super.clone();
		}
		catch (CloneNotSupportedException e){
			throw new AssertionError(e);
		}
		proba910.diceVector = Arrays.copyOf(diceVector, diceVector.length);

		return proba910;
	}
	
	
	
}
