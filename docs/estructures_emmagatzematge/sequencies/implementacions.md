#### Implementacions

Totes les seqüències de dades que hem vist es poden implementar de dues
maneres: com a *vectors dinàmics* o com a *llistes enllaçades*. La
implementació que utilitzem pot afectar al rendiment de determinades
operacions, però no a quines operacions es poden realitzar.

##### Implementació com a llista doblament enllaçada

Una **llista enllaçada** consisteix en una referència o punter a l'element
que ocupa la primera posició. A més, cadascun dels elements manté una
referència a l'element següent, de manera que es pot recuperar qualsevol
dels elements a base de recórrer les referències.

Si cada element guarda referències tant a l'element següent com a
l'element anterior, i la llista enllaçada emmagatzema una referència al
primer i a l'últim elements, diem que es tracta d'una **llista doblament
enllaçada**:

![Llista doblament enllaçada](../imatges/llista_doblament_enllacada.png)

La implementació principal de Java de les llistes doblament enllaçades
es diu *LinkedList*. Una *LinkedList* implementa tant *List* com
*Deque*, així que es pot utilitzar com una pila, una cua, o una llista.

##### Implementació com a vector dinàmic

En la implementació com a vector, la seqüència és simplement un vector,
i tots els elements es troben a posicions contigües de memòria:

![Vector dinàmic](../imatges/vector_dinamic.png)

Quan es necessita reservar memòria per més elements, es reserva un espai
contigu de memòria més gran, es copien tots els elements que hi havia al
vector anterior, i els elements nous. Habitualment, es reserva un espai
de memòria superior al necessari, per exemple del doble del vector
anterior, per tal de no haver de fer aquesta operació per cada element
nou que s'hagi d'emmagatzemar a la seqüència.

Quan s'extreuen elements, el vector no se sol reduir, perquè se suposa
que si en algun moment de l'execució ha arribat a un cert nivell, és
probable que hi torni a arribar, i així s'estalvia el temps de copiar
les dades de nou.

*ArrayDeque* i *ArrayList* són implementacions de Java dels vectors
dinàmics. *ArrayDeque* implementa la interfície *Deque*, mentre que
*ArrayList* implementa la interfície *List*.

##### Les implementacions de *List*

En Java s'han desenvolupat les dues implementacions bàsiques possibles:
com a vector dinàmic, a la classe *ArrayList*, i com a llista doblament
enllaçada, a la classe *LinkedList*. Les classes *Stack* i *Vector*
també implementen *List*, però no es recomana el seu ús. A l'ajuda de la
interfície *List* podeu veure que hi ha més classes que la implementen,
però són de propòsit més específic.

Les classes *AbstractList* i *AbstractSequentialList* no implementen
completament una llista, sinó que donen una part del codi comuna a
moltes implementacions, de manera que si volem afegir una nova
implementació de *List*, derivaríem d'una o l'altra i només hauríem
d'escriure les parts que hi falten. No podem, per tant, crear
directament un objecte de tipus *AbstractList*, sinó que ho hauríem de
fer d'una de les classes derivades que tinguessin la implementació
completa.

Nosaltres ens centrarem en les classes *ArrayList* i *LinkedList*:

![Diagrama classes ArrayList i LinkedList](../imatges/arraylist_linkedlist.png)

Si observem les ajudes d'aquestes dues classes, veurem que a més de la
interfície *List*, implementen altres interfícies i tenen alguns mètodes
propis.

Tot i així, habitualment guardarem les referencies a objectes de tipus
*ArrayList* i *LinkedList*, en variables de tipus *List*, més
genèriques:

`List llista = new ArrayList();`

o

`List llista = new LinkedList();`

Noteu que calen els imports corresponents per tal que aquest codi
funcioni:

```java
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
```

##### Una interfície, dues implementacions

Fixem-nos amb el que hem aconseguit gràcies a utilitzar una interfície
abans de realitzar la implementació d'una estructura:

 * Hem tingut clar d'entrada de què havia de ser responsable l'estructura,
 és a dir, quins mètodes públics hauria de tenir i què faria cadascun d'ells.
 Podem utilitzar una d'aquestes estructures només coneixent la interfície,
 sense haver de mirar el codi de la implementació concreta.

 * Una aplicació que necessiti una llista pot crear una llista de qualsevol
 tipus i a partir de llavors referenciar-la com a tipus genèric List. Això
 permet que puguem canviar la implementació que volem utilitzar a l'aplicació
 canviant una sola línia de codi: la creació de la llista.

 * Encara més, si un mètode necessita utilitzar una llista que rep per
 paràmetre, no s'ha de preocupar per res de quina implementació s'està
 utilitzant, de manera que el podrem utilitzar sigui quin sigui el tipus de
 llista concret que tinguem.

 * El nostre programa és més tolerant als canvis: si apareix un nou tipus de
 llista el podem passar a utilitzar sense canviar pràcticament la resta del
 programa.

![Diagrama de classes, relació ArrayList, LinkedList i List](../imatges/dues_implementacions.png)

Per exemple:

```java
public class ProvaPila {
   public static void main(String[] args) {
       // Creem la pila
       Deque pila = new ArrayDeque();
       comprovaPila(pila);
   }

   public static void comprovaPila(Deque pila) {
       // Provem inserció
       for (int i=0; i<100; i++)
           pila.addFirst(new Integer(i));
...
```

Aquí podem veure com el mètode *comprovaPila* no sap sobre quin tipus de
pila s'està treballant, i les pot tractar totes de la mateixa manera. Si
canviem la creació de la pila per:

```java
Deque pila = new LinkedList();
```

no hem de modificar el mètode *comprovaPila*.

Estem veient dos conceptes bàsics de la programació orientada a objectes
en acció:

 * L'**encapsulació**: per una banda, a *ArrayDeque* o *LinkedList* s'ha
 encapsulat el funcionament intern de l'estructura, de manera que no podem
 manipular-la des de fora si no és a través dels mètodes públics que ens
 proporcionen. Però a més, hem encapsulat els diversos tipus de piles que es
 poden fer sota una interfície comuna, de manera que qui les utilitza les pot
 tractar totes de la mateixa manera i sense conèixer el seu tipus concret.

 * El **polimorfisme**: l'objecte pila de l'exemple anterior és a la vegada
 una *ArrayDeque* i una *Deque*. Podem emmagatzemar una referència a aquest
 objecte tant en variables de tipus *Deque* com en variables de tipus
 *ArrayDeque*.

Seguirem treballant sobre aquestes dues idees més endavant.
