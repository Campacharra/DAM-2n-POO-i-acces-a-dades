## Diagrames de seqüència

### Exercicis

1. En una biblioteca es vol portar un control dels llibres existents,
dels socis de la biblioteca i dels préstecs que s’han dut a terme.

    De cada llibre poden existir un o molts exemplars, així com cada
    exemplar s’haurà de trobar en un catàleg. Per això, es podrà donar
    entrada a un exemplar al catàleg o donar-li sortida.

    A l’hora d’efectuar el préstec caldrà validar que existeix el soci
    que el demana i la disponibilitat de l’exemplar. Les accions a fer
    podran ser la de prestar un llibre i la de retornar-lo.

    Es demana realitzar el diagrama de seqüència per a aquest enunciat.

2. Per a l'exercici de la fàbrica de naus, fes un diagrama de seqüencia
que mostri els participants i els missatges que es realitzen en la
construcció d'una nau amb un motor.

3. Per a l'exercici del zoo, fes un diagrama de seqüència que exemplifiqui
la resposta del sistema a una comanda “mira”. En aquest exemple concret,
suposarem que la comanda resulta en un animal realitzant una acció,
que l'animal resulta ser un cocodril, que l'acció que fa és alimenta,
i que l'altre animal que troba resulta ser una vaca.
