## Exemple de cas d'ús

### Cas d'ús: compra a través de la web

**Resum**: El comprador sol·licita alguns productes a través de la nostra web.
Cal enviar-li els objectes que ha comprat i cal gestionar el pagament.

**Prerequisits**: tenim les dades del comprador.

**Garanties**: s'ha enviat la comanda al transportista. El comprador ha pagat.

**Disparador**: el comprador es connecta a la tenda virtual.

#### Escenari exitós principal

1. El comprador es valida a la pàgina web.
2. El comprador cerca els productes al catàleg i els afegeix al carretó.
3. El comprador revisa el carretó i confirma la compra.
4. La web mostra un resum de la compra, preus, opcions d'enviament, etc.
5. El comprador tria una opció d'enviament.
6. El comprador tria un sistema de pagament.
7. El sistema redirigeix el pagament a l'entitat que correspongui.
8. L'entitat bancària confirma el pagament.
9. El sistema enregistra la compra a magatzem, per tal de preparar els productes.
10. El sistema enregistra la compra al transportista.
11. El sistema envia un email de confirmació amb les dades de la compra.
12. El sistema mostra un resum amb les dades de la compra i permet imprimir
la factura.

#### Variacions

3a. Algun dels productes no es pot enviar per falta d'estoc.  
3a1. Cal modificar la comanda i demanar confirmació.

5a. El comprador vol enviar els productes a una adreça que no és la seva
adreça habitual.  
5a1. Cal oferir la possibilitat d'introduir una nova adreça.

6a. El comprador vol pagar amb una targeta nova.  
6a1. Cal oferir la possibilitat d'introduir les dades d'una nova targeta.

7a. No es pot contactar amb l'empresa de pagament.  
7a1. Tornar a provar o passar a escenari 8a1.

8a. L'empresa de pagament no accepta el pagament.  
8a1. Cal donar l'opció de triar un altre sistema de pagament.

9a. No es pot contactar amb el transportista.  
9a1. Cal registrar la incidència per tal de procedir a l'enviament més tard.

#### Sub-variacions

1. El comprador pot pagar a través de targeta de crèdit, targeta de dèbit o
paypal.

#### Casos d'ús subordinats

- Cerca al catàleg
- Gestió del carretó
- Pagament per cadascun dels sistemes

#### Actors

**Actor principal**: client

**Actors secundaris**: empresa de targetes de crèdit o pagament online,
empresa de missatgeria.

#### Casos d'ús relacionats

- Anul·lació de compres.
- Reclamació i retorn de productes.

#### Diagrama de casos d'ús

En aquest diagrama apareix aquest cas d'ús i els seus casos d'ús relacionats
(sense desenvolupar):

![Exemple diagrama casos d'ús](docs/diagrames_uml/imatges/exemple_cas_dus.png)
